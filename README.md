# RaspberryPiArduinoRunner

Make a Runner to flash Arduino during the build process

The idea is to install this runner on an Arduino, register it on a Gitlab Server

And then be able to flash the arduino plug on the specify port

This way, a code that pass the previous build steps can be install on the Arduino
